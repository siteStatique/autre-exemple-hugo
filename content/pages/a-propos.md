---
title: À propos
date: 2022-05-06
---
Bienvenue sur le site web de démonstration du _démarreur_ (_starter_ en anglais) créé pour la formation [Débugue tes humanités](https://debugue.ecrituresnumeriques.ca).

Les sources de ce site web sont disponibles sur le dépôt suivant : [https://gitlab.huma-num.fr/ecrinum/demarreur](https://gitlab.huma-num.fr/ecrinum/demarreur).

Contact : Antoine Fauchié, [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca), [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca).
